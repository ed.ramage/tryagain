// __tests__/fetch.js
import React from 'react'
import {
  render,
  fireEvent,
  cleanup,
  waitForElement,
} from '@testing-library/react'
// this adds custom jest matchers from jest-dom
import 'jest-dom/extend-expect'

// the mock lives in a __mocks__ directory
// to know more about manual mocks, access: https://jestjs.io/docs/en/manual-mocks
import axiosMock from 'axios'
import App from '../App'

// automatically unmount and cleanup DOM after the test is finished.
afterEach(cleanup)

test('App renders', async () => {
  // Arrange
  axiosMock.get.mockResolvedValueOnce({data: {greeting: 'hello there'}})
  const url = '/greeting'
  const {getByText, getByTestId, container, asFragment} = render(
    <App />,
  )

  // Act //nope
  //fireEvent.click(getByText(/load greeting/i))

  // Let's wait until our mocked `get` request promise resolves and
  // the component calls setState and re-renders.
  // getByTestId throws an error if it cannot find an element with the given ID
  // and waitForElement will wait until the callback doesn't throw an error
  const appTestId = await waitForElement(() =>
    getByTestId('etr_id'),
  )
console.log(appTestId);
  // Assert
//   expect(axiosMock.get).toHaveBeenCalledTimes(1)
//   expect(axiosMock.get).toHaveBeenCalledWith(url)
//   expect(getByTestId('greeting-text')).toHaveTextContent('hello there')
//   expect(getByTestId('ok-button')).toHaveAttribute('disabled')
//   // snapshots work great with regular DOM nodes!
//   expect(container.firstChild).toMatchSnapshot()
//   // you can also get a `DocumentFragment`, which is useful if you want to compare nodes across renders
//   expect(asFragment()).toMatchSnapshot()
})