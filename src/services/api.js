//import { config } from '../config/';
const API_URL = 'http://localhost:8081/api/v1'//process.env.API_SERVER_URI

const callApi = (method = 'GET', endpoint, payload) => {
  const url = API_URL + endpoint
  console.log(url)
  switch (method) {
    case 'GET': {
    return  fetch(url)
    }
  }
};

export const getProjects = (user_id) => callApi('GET', `/projects/${user_id}/`);