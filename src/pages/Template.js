import React from 'react';
import { useHeight } from './hooks';
import { palette } from '../theme/';
import './index.css';

const Template = ({children, theme}) => {
    const height = useHeight();
    const borderCollie = theme.border2;
    const baseBgc = theme.panel1;
    return (
      <div style={{overflowY: 'hidden', height: height, borderTop: `10px solid ${borderCollie}`, borderLeft: `10px solid ${borderCollie}`, backgroundColor: baseBgc}} 
           className="page_template_container">
        {children}      
      </div>
      )
};

export default Template;
