import React from 'react';
import Template from '../Template';
import EditorHead from '../../components/editor/EditorHead';
import RoomList from '../../components/editor/RoomList';


const EditorView = ({theme}) => {
  return (
      <Template theme={theme}>
        <EditorHead theme={theme}/>
        <RoomList theme={theme} />
      </Template>
  )
};

export default EditorView;

