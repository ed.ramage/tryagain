
import React, {useEffect, useState} from 'react';

export const useHeight = () => {

    const getHeight = () => window.innerHeight;

    const [height, setHeight] = useState(getHeight());


    useEffect(() => {
        const resize = () => setHeight(getHeight());
        window.addEventListener('resize', resize)
    
    return () => window.removeEventListener('resize', resize)},
    []);
    
    return height;
};



