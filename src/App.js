import React, { useState, useMemo, useEffect } from 'react';
import {getProjects} from './services/api';
// redux
import {useSelector} from 'react-redux';

// styling
import './App.css';
import { palette } from './theme/';
import EditorView from './pages/Editor';
function App() {
  const theme_options = useMemo(() => palette?.themes, [palette]);
  const [theme, setTheme] = useState(palette[theme_options[0]]);
  const [editorMode, setEditorMode] = useState(true);

  const toggleEditor = () => setEditorMode(!editorMode);

  // trying to implement a quick and dirty authentication system with useEffect.
  // Not sure how to test, might be a waste of time
  useEffect(() => {
   // window.history.pushState(null, null, "i-could-use-this")
   getProjects('1234').then(r => r.json()).then(r => console.log(r));
   return () => {
    alert('app dismounted');
    }  }, []);

  // TODO manage selectors properly
  const currentProjectId = useSelector(state => state.editor.project.current_project_id);


  return (
    <>
            <EditorView theme={theme} test_id="etr_id" />
            <div className="editor_mode_switch">
              <button onClick={toggleEditor}>ToggleEditorMode</button>
              <em style={{paddingLeft: '0.5rem'}}>{editorMode && `editing${currentProjectId ? currentProjectId : '...'}`}</em>
            </div>
     </>
  );
}

export default App;
