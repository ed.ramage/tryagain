import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import s from './redux/store';
import './index.css';
import App from './App';

ReactDOM.render(
  <Provider store={s}>
      <App />
  </Provider>,
  document.getElementById('root'));
