const uuidv4 = require('uuid/v4');

export const createId = () => uuidv4();
