import { combineReducers } from 'redux';
import { editor } from '../ducks/Editor/';
import { user } from '../ducks/User';
export default combineReducers({
    editor,
    user
});
