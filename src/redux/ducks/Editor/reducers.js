import { combineReducers } from 'redux';
import * as types from './types';

const INIT_PROJECT={
    project_ids:['1234'],
    current_project_id: '1234',
    rooms: [],
    room_ids: [],
    templates: [],
    template_ids: []
}

const INIT_CONFIG = {
  rewind_limit: 10,
  rewind_distance: 0
}

const config = (state = INIT_CONFIG, action) => {
  switch(action.type){
    case types.REWIND:
        return {...state, rewind_distance: state.rewind_distance +1};
    default:
        return state;
  }
};

const project = (state = INIT_PROJECT, action) => {
  switch(action.type){
    case types.ADD_ROOM:
        return {...state, rooms: [...state.rooms, action.payload]};
    default:
        return state;
  }
};

export default combineReducers({
  config,
  project
});