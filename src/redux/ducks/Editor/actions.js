import * as types from './types';
import { createId } from '../../../util/createId';

// TODO: have a range of room templates to choose from
const exampleRoom = {
    label: 'blank room',
    description: 'add room objects by clicking the editor',
    connections: [
      {
       'north': {label: 'exit', room_id: 'exit'}
      }]
}

export const createRoom = () => {
  const room = {...exampleRoom, id: createId()}; 
  return ({
    type: types.ADD_ROOM,
    payload: room
  });
}

