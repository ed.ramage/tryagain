export const palette = {
    themes: ['clarence'],
    clarence: {
        border1: '#000000',
        border2: '#003f30',
        border3: '#585656',
        panel1: '#eeb9ab',
        panel2: '#eae0d4'
    }
}
