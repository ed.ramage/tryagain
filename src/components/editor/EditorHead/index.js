import React, { useCallback } from 'react';
import './index.css';
import { useDispatch, useSelector } from 'react-redux';
import { createRoom } from '../../../redux/ducks/Editor/actions';
import { getUser } from '../../../redux/ducks/User/selectors';

const Btn = ({label, onClick, backgroundColor, color}) => <div onClick={onClick} style={{backgroundColor}}><em style={{color: color}}>{label}</em></div>

const EditorHead = ({theme}) => {
  const user = useSelector(getUser);
  const reduxDispatch = useDispatch();
  const createNewRoom = useCallback(() => reduxDispatch(createRoom()), [reduxDispatch]);
  const bgc = theme.border2;
  const tstng = () => alert(`hi ${user?.firstname}`)
  return (
    <div>
      <div test_id="test_editor_head" className="editor_head_title" style={{backgroundColor: bgc}}>
        <h1 style={{color: theme.panel2, paddingLeft: '1rem', marginBottom: 0}}><em>editor header</em></h1>
      </div>
      <div test_id="test_editor_head_buttonbar" className="editor_head_buttonbar">
        <Btn label="Add Room" onClick={createNewRoom} backgroundColor={bgc} color={theme.panel2}/>
        <Btn label="Add Object" onClick={tstng} backgroundColor={bgc} color={theme.panel2}/>
        <Btn label="View Map" onClick={tstng} backgroundColor={bgc} color={theme.panel2}/>
      </div>
    </div>
  )
};

export default EditorHead;
