import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { getRooms } from '../../../redux/ducks/Editor/selectors';

const RoomList = ({theme}) => {
  
  const rooms = useSelector(getRooms);
 console.log('rooms: ', rooms) 
  return (
    <div style={{backgroundImage: `linear-gradient(${theme.panel1}, ${theme.panel2})`, height: '100%', width: '10rem'}}>
       {rooms?.length
         ? rooms.map(r => <div><h3>{r?.label}</h3><p>{r?.id}</p></div>)
         : <p>no rooms yet</p>
      } 
    </div>
  )
};

export default RoomList;
